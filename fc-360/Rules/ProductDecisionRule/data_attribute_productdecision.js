function data_attribute_productdecision(payload) {
    var cashflowData = payload.cashflowVerificationData[0].SelectedAccountData[0].CashFlow;
    var applicationeligibility = payload.eligibility[0];
    var application = payload.application[0];
    var personalCreditReport = payload.personalCreditReport[0];
    var starterMCA = {
        'CreditScore': 500,
        'AverageRevenue': 6000,
        'TimeinBusiness': 9,
        'NegetiveDays': 6,
        'Deposits': 5,
        'DailyBalance': 1000,
        'TaxLiens': 'Payment Plan',
        'PersonalBankruptcies': 3,
        'BusinessBankruptcies': 5
    };
    var traditionalMCA = {
        'CreditScore': 500,
        'AverageRevenue': 20000,
        'TimeinBusiness': 12,
        'NegetiveDays': 10,
        'Deposits': 5,
        'DailyBalance': 1000,
        'TaxLiens': 'Payment Plan',
        'PersonalBankruptcies': 3,
        'BusinessBankruptcies': 5
    };
    var streamLineLoc = {
        'CreditScore': 575,
        'AverageRevenue': 25000,
        'TimeinBusiness': 12,
        'NegetiveDays': 6,
        'Deposits': 10,
        'DailyBalance': 2500,
        'TaxLiens': 'Payment Plan',
        'PersonalBankruptcies': 3,
        'BusinessBankruptcies': 5
    };
    var premiumLoc = {
        'CreditScore': 650,
        'AverageRevenue': 35000,
        'TimeinBusiness': 24,
        'NegetiveDays': 3,
        'Deposits': 10,
        'DailyBalance': 2500,
    };
    var platinumLoc = {
        'CreditScore': 680,
        'AverageRevenue': 35000,
        'TimeinBusiness': 24,
        'NegetiveDays': 0,
        'Deposits': 10,
        'DailyBalance': 2500,
    };
    var otherLender = {
        'TimeinBusiness': 24,
        'NegetiveDays': 12,
        'Deposits': 10,
        'DailyBalance': 2500
    };
    var currentproductObjectOriginal = {
        'CreditScore': personalCreditReport.CreditScore,
        'AverageRevenue': (application.annualRevenue / 3),
        'TimeinBusiness': applicationeligibility.TIB,
        'NegetiveDays': cashflowData.TransactionSummary.NumberOfNSF,
        'Deposits': cashflowData.TransactionSummary.TotalCreditsCount,
        'DailyBalance': cashflowData.TransactionSummary.AverageDailyBalance,
        'TaxLiens': 'Payment Plan',
        'PersonalBankruptcies': 3,
        'BusinessBankruptcies': 5
    };
    var currentproductObject = {
        'CreditScore': personalCreditReport.CreditScore,
        'AverageRevenue': (application.annualRevenue / 3),
        'TimeinBusiness': applicationeligibility.TIB,
        'NegetiveDays': cashflowData.TransactionSummary.NumberOfNSF,
        'Deposits': cashflowData.TransactionSummary.TotalCreditsCount,
        'DailyBalance': cashflowData.TransactionSummary.AverageDailyBalance,
        'TaxLiens': 'Payment Plan',
        'PersonalBankruptcies': 3,
        'BusinessBankruptcies': 5,
        'TimeinBusinessText': applicationeligibility.TimeInBusiness,
    };
    var StarterMCAOffer = {
        'MinAmount': 2500,
        'MaxAmount': 5000,
        'MinFactor': 1.45,
        'MaxFactor': 1.45,
        'MinDuration': 4,
        'MaxDuration': 4,
        'FunderFee': 295,
        'Comp': 15,
        'MaxGross': 20,
        'PSF': 0
    };
    var TraditionalMCAOffer = {
        'MinAmount': 5000,
        'MaxAmount': 75000,
        'MinFactor': 1.24,
        'MaxFactor': 1.34,
        'MinDuration': 4,
        'MaxDuration': 6,
        'FunderFee': 3,
        'Comp': 0,
        'MaxGross': 20,
        'PSF': 2
    };
    var StreamLineLocOffer = {
        'MinAmount': 5000,
        'MaxAmount': 50000,
        'MinFactor': 1.35,
        'MaxFactor': 1.44,
        'MinDuration': 6,
        'MaxDuration': 6,
        'FunderFee': 3,
        'Comp': 8,
        'MaxGross': 18,
        'PSF': 0
    };
    var PremiumLocOffer = {
        'MinAmount': 5000,
        'MaxAmount': 50000,
        'MinFactor': 1.29,
        'MaxFactor': 1.29,
        'MinDuration': 6,
        'MaxDuration': 6,
        'FunderFee': 3,
        'Comp': 7,
        'MaxGross': 18,
        'PSF': 0
    };
    var PletinumLocOffer = {
        'MinAmount': 5000,
        'MaxAmount': 50000,
        'MinFactor': 1.25,
        'MaxFactor': 1.25,
        'MinDuration': 6,
        'MaxDuration': 6,
        'FunderFee': 3,
        'Comp': 5,
        'MaxGross': 15,
        'PSF': 0
    };
    var AverageRevenue = currentproductObject.AverageRevenue;
    var CreditScore = currentproductObject.CreditScore;
    var TimeinBusiness = currentproductObject.TimeinBusiness;
    var NegetiveDays = currentproductObject.NegetiveDays;
    var Deposits = currentproductObject.Deposits;
    var DailyBalance = currentproductObject.DailyBalance;
    var TaxLiens = currentproductObject.TaxLiens;
    var PersonalBankruptcies = currentproductObject.PersonalBankruptcies;
    var BusinessBankruptcies = currentproductObject.BusinessBankruptcies;
    var Data = null;
    var result = 'Failed';
    var Productresult = {
        'productResult': Data
    };
    try {
        if (cashflowData == null) {
            return {
                'result': result,
                'detail': null,
                'data': Productresult,
                'rejectcode': '',
                'exception': ['Cashflow details not found']
            };
        }
        if (applicationeligibility == null) {
            return {
                'result': result,
                'detail': null,
                'data': Productresult,
                'rejectcode': '',
                'exception': ['applicationeligibility details not found']
            };
        }
        if (personalCreditReport == null) {
            return {
                'result': result,
                'detail': null,
                'data': Productresult,
                'rejectcode': '',
                'exception': ['personalCreditReport details not found']
            };
        }
        if (application == null) {
            return {
                'result': result,
                'detail': null,
                'data': Productresult,
                'rejectcode': '',
                'exception': ['application details not found']
            };
        }
        if (payload != null && payload != undefined) {
            if (AverageRevenue >= platinumLoc.AverageRevenue && CreditScore >= platinumLoc.CreditScore && TimeinBusiness >= platinumLoc.TimeinBusiness && NegetiveDays <= platinumLoc.NegetiveDays && Deposits >= platinumLoc.Deposits && DailyBalance >= platinumLoc.DailyBalance) {
                result = 'Passed';
                Data = {
                    'platinumLoc': currentproductObject,
                    'ProductId': 'mcaloc',
                    'SubProductId': 'PlatinumLoc',
                    'ProductInfo': platinumLoc,
                    'Offer': PletinumLocOffer,
                    'CurrentProductObj': currentproductObject
                };
            } else if (AverageRevenue >= premiumLoc.AverageRevenue && CreditScore >= premiumLoc.CreditScore && TimeinBusiness >= premiumLoc.TimeinBusiness && NegetiveDays <= starterMCA.NegetiveDays && Deposits >= starterMCA.Deposits && DailyBalance >= premiumLoc.DailyBalance) {
                result = 'Passed';
                Data = {
                    'PremiumLoc': currentproductObject,
                    'ProductId': 'mcaloc',
                    'SubProductId': 'PremiumLoc',
                    'ProductInfo': premiumLoc,
                    'Offer': PremiumLocOffer,
                    'CurrentProductObj': currentproductObject
                };
            } else if (AverageRevenue >= streamLineLoc.AverageRevenue && CreditScore >= streamLineLoc.CreditScore && TimeinBusiness >= streamLineLoc.TimeinBusiness && NegetiveDays <= streamLineLoc.NegetiveDays && Deposits >= streamLineLoc.Deposits && DailyBalance >= streamLineLoc.DailyBalance && PersonalBankruptcies >= streamLineLoc.PersonalBankruptcies && BusinessBankruptcies >= streamLineLoc.BusinessBankruptcies) {
                result = 'Passed';
                Data = {
                    'StreamLineLoc': currentproductObject,
                    'ProductId': 'mcaloc',
                    'SubProductId': 'StreamLineLoc',
                    'ProductInfo': streamLineLoc,
                    'Offer': StreamLineLocOffer,
                    'CurrentProductObj': currentproductObject
                };
            } else if (AverageRevenue >= traditionalMCA.AverageRevenue && CreditScore >= traditionalMCA.CreditScore && TimeinBusiness >= traditionalMCA.TimeinBusiness && NegetiveDays <= traditionalMCA.NegetiveDays && Deposits >= traditionalMCA.Deposits && DailyBalance >= traditionalMCA.DailyBalance && PersonalBankruptcies >= traditionalMCA.PersonalBankruptcies && BusinessBankruptcies >= traditionalMCA.BusinessBankruptcies) {
                result = 'Passed';
                Data = {
                    'TraditionalMCA': currentproductObject,
                    'ProductId': 'mca',
                    'SubProductId': 'TraditionalMCA',
                    'ProductInfo': traditionalMCA,
                    'Offer': TraditionalMCAOffer,
                    'CurrentProductObj': currentproductObject
                };
            } else if (AverageRevenue >= starterMCA.AverageRevenue && CreditScore >= starterMCA.CreditScore && TimeinBusiness >= starterMCA.TimeinBusiness && NegetiveDays <= starterMCA.NegetiveDays && Deposits >= starterMCA.Deposits && DailyBalance >= starterMCA.DailyBalance && PersonalBankruptcies >= starterMCA.PersonalBankruptcies && BusinessBankruptcies >= starterMCA.BusinessBankruptcies) {
                result = 'Passed';
                Data = {
                    'StarterMCA': currentproductObject,
                    'ProductId': 'mca',
                    'SubProductId': 'StarterMCA',
                    'ProductInfo': starterMCA,
                    'Offer': StarterMCAOffer,
                    'CurrentProductObj': currentproductObject
                };
            } else {
                result = 'Passed';
                Data = {
                    'StarterMCA': currentproductObject,
                    'ProductId': 'mca',
                    'SubProductId': 'StarterMCA',
                    'ProductInfo': starterMCA,
                    'Offer': StarterMCAOffer,
                    'CurrentProductObj': currentproductObject
                };
            }
        } else {
            result = 'Passed';
            Data = {
                'StarterMCA': currentproductObject,
                'ProductId': 'mca',
                'SubProductId': 'StarterMCA',
                'ProductInfo': starterMCA,
                'Offer': StarterMCAOffer,
                'CurrentProductObj': currentproductObject
            };
            Productresult = {
                'productResult': Data
            };
        }
        Productresult = {
            'productResult': Data
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify' + e.message],
            'data': Productresult,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}