function default_offer(payload) {
    if (payload != null && typeof (payload) != 'undefined') {
        var product = payload.product[0];
        var StarterMCAOffer = {
            'MinAmount': 2500,
            'MaxAmount': 5000,
            'MinFactor': 1.45,
            'MaxFactor': 1.45,
            'MinDuration': 4,
            'MaxDuration': 4,
            'FunderFee': 295,
            'Comp': 15,
            'MaxGross': 20,
            'PSF': 0
        };
        var TraditionalMCAOffer = {
            'MinAmount': 5000,
            'MaxAmount': 75000,
            'MinFactor': 1.24,
            'MaxFactor': 1.34,
            'MinDuration': 4,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 0,
            'MaxGross': 20,
            'PSF': 2
        };
        var StreamLineLocOffer = {
            'MinAmount': 5000,
            'MaxAmount': 50000,
            'MinFactor': 1.35,
            'MaxFactor': 1.44,
            'MinDuration': 6,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 8,
            'MaxGross': 18,
            'PSF': 0
        };
        var PremiumLocOffer = {
            'MinAmount': 5000,
            'MaxAmount': 50000,
            'MinFactor': 1.29,
            'MaxFactor': 1.29,
            'MinDuration': 6,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 7,
            'MaxGross': 18,
            'PSF': 0
        };
        var PletinumLocOffer = {
            'MinAmount': 5000,
            'MaxAmount': 50000,
            'MinFactor': 1.25,
            'MaxFactor': 1.25,
            'MinDuration': 6,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 5,
            'MaxGross': 15,
            'PSF': 0
        };

        var Data = null;
        var result = 'Failed';
        var Productresult = {
            'productResult': Data
        };
        try {
            if (product == null) {
                return {
                    'result': result,
                    'detail': null,
                    'data': Productresult,
                    'rejectcode': '',
                    'exception': ['application details not found']
                };
            }
            var productId = product.ProductId;
            if (productId == 'mca') {
                result = 'Passed';
                Data = {
                    'Offer': TraditionalMCAOffer,
                    'SubProductId': 'TraditionalMCA'
                };
            } else {
                result = 'Passed';
                Data = {
                    'Offer': PletinumLocOffer,
                    'SubProductId': 'PremiumLoc'
                };
            }

            Productresult = {
                'productResult': Data
            };
            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': ['Unable to verify' + e.message],
                'data': Productresult,
                'rejectcode': '',
                'exception': [e.message]
            };
        }
    }
}