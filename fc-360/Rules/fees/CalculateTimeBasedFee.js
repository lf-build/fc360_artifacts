function CalculateTimeBasedFee(payload) {
	if(payload == null || payload.EventData == null){
		return 0;
	}
	var feeAmount = 0;
	var dsla = payload.EventData.DSLA;
	switch(dsla) {
		case 30:
			feeAmount = 5;
			break;
		case 60:
			feeAmount = 10;
			break;
		case 90:
			feeAmount = 15;
			break;
		default:
			break;
	}
	return feeAmount;
}