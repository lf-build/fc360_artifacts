function verification_rule_businessdeliquencyverification_paynet(payload) {
	var objData = null;
	var paynetResult = null;
	try {
		var result = 'Passed';
		if (payload != null && payload != undefined) {
			if (payload.paynetReport != null && payload.paynetReport != undefined) {
				objData = payload.paynetReport[0];
				if (objData.paynetResult && objData.paynetResult == 'no match') {
					result = 'Failed';
				} else if (objData != null && objData.Past_due_31_plus_occurrences) {
					if (objData.Past_due_31_plus_occurrences >= 30) {
						result = 'Failed';
					}
				}
			}
		}
		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': 'Unable to verify',
			'data': objData,
			'rejectcode': '',
			'exception': [e.message]
		};
	}
}