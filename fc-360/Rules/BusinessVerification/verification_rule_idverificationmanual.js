function verification_rule_idverificationmanual(payload) {
    var Name = '';
    var DrivingLicence = '';
    var DateOfBirthMatch = '';
    var result = 'Failed';
    function VerifyInput(payload) {
        try {
            if (payload != null && payload != undefined) {
                var objData = payload.idVerificationData[0];
                Name = objData.nameMatch;
                DrivingLicence = objData.drivingLicenceMatch;
                DateOfBirthMatch = objData.dateOfBirthMatch;
                if (Name == true && DrivingLicence == true && DateOfBirthMatch == true) {
                    result = 'Passed';
                }
            }
            var Data = {
                'Name': Name,
                'DrivingLicence': DrivingLicence,
                'DateOfBirthMatch': DateOfBirthMatch
            };
            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': 'Unable to verify',
                'data': objData,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    return VerifyInput(payload);
}