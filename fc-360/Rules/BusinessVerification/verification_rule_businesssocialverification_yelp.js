function verification_rule_businesssocialverification_yelp(payload) {
    var objData = null;
    try {
        var result = 'Passed';

        if (payload != null && payload != undefined) {

            objData = payload.yelpReport[0];
            if (objData != null && objData != '') {
                if (objData.Rating <= 1 || objData.IsClosed == true) {
                    result = 'Failed';
                }
            }
        }

        return {
            'result': result,
            'detail': null,
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    }
}
