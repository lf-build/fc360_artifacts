function verification_rule_tloxp_person_failure_search(payload) {
    var objData = null;
    try {
        var result = 'Passed';
        if (payload != null && payload != undefined) {
            objData = payload.tloxpPersonReport[0];
            var d = new Date();
            var n = d.getFullYear();
            var latestbankruptcyyear=n-Number(objData.Bankruptcies.FileDate.Year);
            if (objData != null && objData != '') {
                
                if (objData.CreditScore==null||objData.CreditScore<5||objData.CreditScore==''||latestbankruptcyyear<3) {
                    result = 'Failed';
                }
            }
        }
        return {
            'result': result,
            'detail': null,
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    }
}
