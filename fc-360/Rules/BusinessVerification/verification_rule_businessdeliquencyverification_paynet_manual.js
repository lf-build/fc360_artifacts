function verification_rule_businessdeliquencyverification_paynet_manual(payload) {
    var objData = null;
    try {
        var result = 'Failed';
        if (payload != null && payload != undefined) {
            objData = payload.businessDeliquencyVerificationData[0];
            if (objData != null && objData != '' && objData.IsVerificationDone == true) {
                result = 'Passed';
            }
        }
        return {
            'result': result,
            'detail': null,
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': 'Unable to verify',
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    }
}