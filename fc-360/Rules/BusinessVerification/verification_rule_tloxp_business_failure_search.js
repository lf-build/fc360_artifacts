function verification_rule_tloxp_business_failure_search(payload) {
    var objData = null;
    try {
        var result = 'Passed';
        if (payload != null && payload != undefined) {
            var lookupService = self.call('lookup');
            objData = payload.tloxpBusinessReport[0];
            var totalmonths=0;
            var latestbankruptcyyear=0;
            if (objData != null && objData != '') {
                if(objData.SIC!=''){
                    result = checkSIC(lookupService, SIC);
                }
                var d = new Date();
                var n = d.getFullYear();
                if(objData.IncorporationDate!=null){
                var monthsfromyear=(n-Number(objData.IncorporationDate.Year))*12;
                    totalmonths=monthsfromyear+Number(objData.IncorporationDate.Month);
                }
                if(objData.BankruptcyFilings!=null){
                    latestbankruptcyyear=n-Number(objData.BankruptcyFilings.FilingDate.Year);
                }
                if (objData.SIC==''||totalmonths <9|| objData.CreditScore<5||objData.CreditScore==''||latestbankruptcyyear<5) {
                    result = 'Failed';
                }
              
            }
        }
        return {
            'result': result,
            'detail': null,
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    }
    function checkSIC(lookupService, SIC) {
        var result='';
        lookupService.get('prohibitedSIC').then(function(response) {
            var lookupResult = response;
            if (lookupResult != null && lookupResult != undefined) {
                for (var key in lookupResult) {
                    if (SIC != null && SIC != '' && SIC == lookupResult[key]) {
                        result = 'Failed';
                        break;
                    }
                }
                ;
            }
        });
        return result;
    }
}
