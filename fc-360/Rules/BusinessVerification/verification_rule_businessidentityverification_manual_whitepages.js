function verification_rule_businessidentityverification_manual_whitepages(payload) {
    try {
        var verificationData = null;
        var result = 'Failed';
        if (payload != null && payload != undefined) {
            var objData = payload.businessIdentityVerificationData[0];
            if (objData != null) {
                verificationData = objData.IsVerificationDone;
                if (verificationData == true) {
                    result = 'Passed';
                }
            }
        }
        var Data = {
            'VerificationData': verificationData
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': 'Unable to verify',
            'data': null,
            'rejectcode': '',
            'exception': []
        };
    }
}