function verification_rule_sicverification(payload) {
	var self = this;
	var result = 'Passed';
	var sic1 = null;
	var sic2 = null;
	var lookupResult = {};
	var sicMatches = '';
	var lookupService = self.call('lookup');
	try {
		if (payload != null && payload != undefined) {
			var objData = payload.bizApiSearchReport[0];
			if (objData != null && objData != '') {
				sic1 = objData.FourDigitSIC1;
				sic2 = objData.FourDigitSIC2;
			}
			return lookupService.get('prohibitedSIC').then(function (response) {
				lookupResult = response;

				if (lookupResult != null && lookupResult != undefined) {
					for (var key in lookupResult) {
						if (sic1 != null && sic1 != '' && sic1 == lookupResult[key]) {
							sicMatches = key;
							result = 'Failed';
							break;
						}

						if (sic2 != null && sic2 != '' && sic2 == lookupResult[key]) {
							sicMatches = key;
							result = 'Failed';
							break;
						}
					};
				}
				return {
					'result': result,
					'detail': null,
					'data': sicMatches,
					'rejectcode': '',
					'exception': []
				};
			});
		}
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}
}
