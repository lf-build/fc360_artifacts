function verification_rule_plaidcashflow(payload) {

	var result = 'Passed';
	var IsNumberOfDepositPerMonth = true;
	var IsNumberOfNegativeDays = true;
	var IsNumberOfNSF = true;
	var IsAverageBalance = true;
	var IsMonthlyDepositAmount = true;
	var IsAverageDepositSize = true;

	var MaxNegativeDays = 5;
	var MaxNSF = 3;
	var MinAverageBalance = 10;
	var MinMonthlyDeposit = 10;

	var errorData = [];
	var errorMessage;
	var Data = {};

	if (payload != null) {
		try {			
			var selectedCashflow = payload.cashflowVerificationData.SelectedAccountData;
			if (selectedCashflow!=null) {
				
				if (selectedCashflow.CashFlow != null && selectedCashflow.CashFlow.TransactionSummary != null) {
					var input = selectedCashflow.CashFlow.TransactionSummary;
					if (input != null) {
						if (input.NumberOfNegativeBalance >= MaxNegativeDays) {
							IsNumberOfNegativeDays = false;
							result = 'Failed';
						}
						if (input.NumberOfNSF >= MaxNSF) {
							IsNumberOfNSF = false;
							result = 'Failed';
						}
						if (input.AverageDailyBalance <= MinAverageBalance) {
							IsAverageBalance = false;
							result = 'Failed';
						}
						if (input.AverageDeposit <= MinMonthlyDeposit) {
							IsMonthlyDepositAmount = false;
							result = 'Failed';
						}

						Data = {
							'IsNumberOfDepositPerMonth': IsNumberOfDepositPerMonth,
							'IsNumberOfNegativeDays': IsNumberOfNegativeDays,
							'IsNumberOfNSF': IsNumberOfNSF,
							'IsAverageBalance': IsAverageBalance,
							'IsMonthlyDepositAmount': IsMonthlyDepositAmount,
							'IsAverageDepositSize': IsAverageDepositSize
						};
					}
				}

			} else {
				errorMessage = 'plaidCashflow information is Null : Unable to verify';
				errorData.push(errorMessage);
				result = 'Failed';
			}
		} catch (e) {

			errorMessage = 'Unable to verify : ' + e.message;
			errorData.push(errorMessage);
			result = 'Failed';

		}
	} else {
		result = 'Failed';
		errorMessage = 'Payload is not available';
		errorData.push(errorMessage);
	}

	return {
		'result': result,
		'detail': '',
		'data': Data,
		'rejectcode': '',
		'exception': errorData
	};
}
