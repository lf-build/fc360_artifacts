function BusinessApplicationEligibilityCheck(payload) {
	var Industry = '';
	var IndustryName = '';
	var IsRestricted = false;
	var result = 'Passed';
	var RequestedAmount = 0;
	var TIB = 0;
	var TimeInBusiness = '';
	var State = '';
	var LoanAmountResult = true;
	var TimeInBusinessResult = true;
	var IsUSbasedBusiness = true;
	var BusinessLocation = null;
	var MinRequestedAmountConstant = 2500;
	var MinTimeInBusinessConstant = 9;
	var MinTimeInHomeConstant = 24;
	var BusinessLocationForHomeLookup = '3';
	function CheckEligibility(payload) {
		if (payload != null) {
			var input = payload.application[0];
			if (input != null) {
				Industry = input.industry;
				RequestedAmount = input.requestedAmount;
				BusinessLocation = input.businessLocation;
				if (input.businessAddress != null) {
					State = input.businessAddress.State;
				}
				if (input.businessStartDate != null) {
					TIB = GetMonthDiff(new Date(input.businessStartDate.Time), new Date());
					TimeInBusiness = parseInt(TIB / 12) + ' Years ' + parseInt(TIB % 12) + ' Months'
				} else {
					result = 'Passed';
					TimeInBusinessResult = false;
				}
				if (BusinessLocation === BusinessLocationForHomeLookup) {
					if (TIB < MinTimeInHomeConstant) {
						result = 'Passed';
						TimeInBusinessResult = false;
					}
				} else {
					if (TIB <= MinTimeInBusinessConstant) {
						result = 'Passed';
						TimeInBusinessResult = false;
					}
				}
				if (RequestedAmount < MinRequestedAmountConstant) {
					result = 'Passed';
					LoanAmountResult = false;
				}
				if (IsRestricted == true) {
					result = 'Failed';
				}
			}
		}
		var Data = {
			'LoanAmount': RequestedAmount,
			'LoanAmountResult': LoanAmountResult,
			'TimeInBusinessResult': TimeInBusinessResult,
			'TimeInBusiness': TimeInBusiness,
			'State': State,
			'TIB': TIB
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	}
	function GetMonthDiff(d1, d2) {
		var months;
		months = (d2.getFullYear() - d1.getFullYear()) * 12;
		months -= d1.getMonth() + 1;
		months += d2.getMonth();
		return months <= 0 ? 0 : months;
	}
	return CheckEligibility(payload);
}