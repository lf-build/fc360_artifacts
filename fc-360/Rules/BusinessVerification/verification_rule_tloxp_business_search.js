function verification_rule_tloxp_business_search(payload) {
    var objData = null;
    try {
        var result = 'Passed';
        if (payload != null && payload != undefined) {
            objData = payload.tloxpBusinessReport[0];
            var lookupService = self.call('lookup');
            var totalmonth=0;
            if (objData != null && objData != '') {
                if(objData.SIC!=''){
                    result = checkSIC(lookupService, SIC);
                }
                var d = new Date();
                var n = d.getFullYear();
                if(objData.IncorporationDate!=null){
                var monthsfromyear=(n-Number(objData.IncorporationDate.Year))*12;
                totalmonths=monthsfromyear+Number(objData.IncorporationDate.Month);
               }
                if (totalmonths <9|| objData.CreditScore<5||objData.CreditScore==''||objData.ActiveFromSos!='Active'||objData.ActiveFromSos=='') {
                    result = 'Passed';
                }
            }
        }
        return {
            'result': result,
            'detail': null,
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Passed',
            'detail': ['Unable to verify'],
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    }
    function checkSIC(lookupService, SIC) {
        var result='';
        lookupService.get('restrictedSIC').then(function(response) {
            var lookupResult = response;
            if (lookupResult != null && lookupResult != undefined) {
                for (var key in lookupResult) {
                    if (SIC != null && SIC != '' && SIC == lookupResult[key]) {
                        result = 'Passed';
                        break;
                    }
                }
                ;
            }
        });
        return result;
    }
}
