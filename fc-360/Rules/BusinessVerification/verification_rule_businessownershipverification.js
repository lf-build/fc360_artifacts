function verification_rule_businessownershipverification(payload) {
    var ownershipData = '';
    var result = 'Failed';
    function VerifyInput(payload) {
        try {
            if (payload != null && payload != undefined) {
                var objData = payload.businessOwnershipVerificationData[0];
                if (objData != null) {
                    ownershipData = objData.ownershipDoc;
                    if (ownershipData == true) {
                        result = 'Passed';
                    }
                }
            }
            var Data = {
                'OwnershipData': ownershipData
            };
            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': 'Unable to verify',
                'data': objData,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    return VerifyInput(payload);
}
