function verification_rule_bankstatement(payload) {
	var result = 'Failed';
	var AccountNumber = '';
	var RoutingNumber = '';
	var MinRoutingNumberLength = 9;
	try {
		if (payload != null && payload != undefined) {
			var objData = payload.bankVerificationData[0];
			var fundingData = payload.fundingAccount[0];
			if (fundingData != null && objData != null) {
				AccountNumber = objData.accountMatch;
				RoutingNumber = objData.routingMatch;
				var matchResult = AccountNumber.indexOf(fundingData.AccountNumber);
				if (RoutingNumber.length >= MinRoutingNumberLength) {
					if (matchResult >= 0 && objData.nameMatch == true) {
						result = 'Passed';
					}
				}
			}
		}
		var Data = {
			'AccountNumber': AccountNumber,
			'RoutingNumber': RoutingNumber,
			'NameMatch': objData.nameMatch
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}
}