function taggingRuleForFundingStatusChanged(payload) {

	var applyTags = [];
	var removeTags = [];
	if (payload!=null && payload.eventData!=null && payload.eventData.Data!=null && payload.eventData.Data.Response!=null) {
		var objFundingStatus = payload.eventData.Data.Response;
		
			if (objFundingStatus.RequestStatus.toLowerCase() == 'returned') {
				applyTags.push('ACH Return');
			}
			else if (objFundingStatus.RequestStatus.toLowerCase() == 'reattempt') {
				removeTags.push('ACH Return');
			}
	}
	var Data = {
		'ApplyTags': applyTags,
		'RemoveTags': removeTags
	};
	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
} 