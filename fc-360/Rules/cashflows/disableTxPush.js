function disableTxPush(entityType, entityId) {
	var self = this;
	var cashFlowService = self.call('cashflow');
	var finicityService = self.call('finicity');
	var eventHubService = self.call('eventHub');
	var dataAttributesService = self.call('dataAttribute');
	return dataAttributesService.get('application', entityId, 'finicityInformation').then(function (finicityInformationAttributes) {
		return finicityInformationAttributes[0].customerId;
	}).then(function (customerId) {
		return cashFlowService.getAllAccounts(entityType, entityId).then(function (bankAccounts) {
			if (bankAccounts != null && bankAccounts.accounts != null) {
				var bankFincityAccounts = bankAccounts.accounts.filter(function (el) {
						return el.source.toLowerCase() == 'finicity';
					});
				var promises = [];
				if (bankFincityAccounts != null && bankFincityAccounts.length > 0) {
					bankFincityAccounts.forEach(function (objbankAccount) {
						promises.push(finicityService.disableTxPushCustomerAccount(entityType, entityId, customerId, objbankAccount.providerAccountId).then(function (item) {
								var data = {
									EntityType: entityType,
									EntityId: entityId,
									ProviderAccountId: objbankAccount.providerAccountId,
									AccountNumber: objbankAccount.accountNumber
								};
								return eventHubService.publish('FinicityTxPushDisableAccountSucceed', data);
							}).catch (function () {
								var data = {
									EntityType: entityType,
									EntityId: entityId,
									ProviderAccountId: objbankAccount.providerAccountId,
									AccountNumber: objbankAccount.accountNumber
								};
								return eventHubService.publish('FinicityTxPushDisableAccountFailed', data)
							})
								);
						});
						return Promise.all(promises).then(function () {
							return {
								'status': 'Success'
							}
						});
					}
				}
				return {
					'status': 'Success'
				}
			});
		});
	}