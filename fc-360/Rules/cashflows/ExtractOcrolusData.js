function ExtractOcrolusData(payload) {
	var data = {};
	var result = 'Passed';
	var errorData = [];
	try {
		var lstAccounts = [];
		if (payload != null) {

			var details = payload.eventData.Response.response.bank_accounts;
			var availableBalance = 0;
			if (details != null) {

				var objAccount = {
					'ProviderAccountId': '',
					'AvailableBalance': 0.0,
					'CurrentBalance': 0.0,
					'BankName': '',
					'AccountType': '',
					'AccountNumber': '',
					'Source': ''
				};
				if (details.length > 0) {
					objAccount.AccountNumber = details[0].account_number;
					objAccount.Source = 'Ocrolus';
					objAccount.ProviderAccountId = details[0].pk;
					objAccount.BankName = details[0].name;
					objAccount.AccountType = details[0].account_type;
					objAccount.AvailableBalance = details[0].periods[0].end_balance;
					objAccount.CurrentBalance = details[0].periods[0].end_balance;
				}
			}
			data = objAccount;
		}
	} catch (e) {
		return null;
	}
	return {
		'result': result,
		'detail': null,
		'data': data,
		'rejectcode': '',
		'exception': []
	};
};