 function extractFinicityAccount(input) {
     function convertEPOCDate(d) {
         var date = new Date(d * 1000);
         date = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
         return date;
     }
     if (input != null) {
         var response = input.Response;
         if (response != null) {
             if (response.Account != null) {
                 var account = response.Account;
                 var objAccount = {
                     'ProviderAccountId': '',
                     'AvailableBalance': 0,
                     'CurrentBalance': 0,
                     'BankName': '',
                     'AccountType': '',
                     'AccountNumber': '',
                     'Source': '',
                     'IsCashflowAccount': false,
                     'IsFundingAccount': false,
                     'BalanceDate': ''
                 };
                 objAccount.Source = 'Finicity';
                 objAccount.ProviderAccountId = account.id;
                 objAccount.BankName = account.InstitutionName;
                 objAccount.AccountType = account.type;
                 objAccount.AccountNumber = account.number;
                 if (account.Detail != null && account.Detail.availableBalanceAmount != null) {
                     objAccount.AvailableBalance = account.Detail.availableBalanceAmount;
                 }
                 objAccount.CurrentBalance = account.balance;
                 if (account.RoutingNumber != null && account.RoutingNumber != '') {
                     objAccount.RoutingNumber = account.RoutingNumber;
                 }
                 if (account.realAccountNumber != null && account.realAccountNumber != '') {
                     data.RealAccountNumber = account.realAccountNumber;
                 }
                 if (account.balanceDate != null) {
                     objAccount.BalanceDate = convertEPOCDate(account.balanceDate);
                 }
                 return objAccount;
             }
         }
     }
     return objAccount;
 }