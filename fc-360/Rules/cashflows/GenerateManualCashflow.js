function GenerateManualCashflow(payload) {
    var Finicitymonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    function RunCashFlow(payload) {
        var result = 'Passed';
        var Category_NSF = ['Bank Fee'];
        var Category_Revenue = ['Business Income'];
        var Category_Exclude_Special_Deposite = ['Interest Income', 'Loans'];
        var endingBalance = 0;
        var beginingBalance = 0;
        var StartDate;
        var EndDate;
        var NegativeBalanceCount = 0;
        var allAccountsCashflows = {};
        var calculateRecurringTransaction = true;
        if (typeof(payload) != 'undefined') {
            var cashFlowInput = payload.eventData.Response;
            var account = cashFlowInput.Accounts;
            var myCustomCashFlow = {
                'MonthlyCashFlows': [{}],
                'TransactionSummary': {},
                'CategorySummary': [{}],
                'TransactionList': [{}],
                'RecurringList': [{}],
                'MCARecurringList': [{}]
            };
            var FinicityCashFlowViewModel = {
                'AccountHeader': '',
                'AccountID': '',
                'AccountNumber': '',
                'InstitutionName': '',
                'AccountType': '',
                'referenceNumber': '',
                'IsSelected': false,
                'Source': '',
                'CashFlow': myCustomCashFlow
            };
            var cashFlowResult = FinicityCashFlowViewModel;
            endingBalance = account.CurrentBalance;
            beginingBalance = endingBalance;
            var SelectedTransactions = [];
            var MonthlyCashFlowsList = [];
            var CategorySummaryList = [];
            var TransactionList = [];
            var RecurringList = [];
            var MCARecurringList = [];
            var LastFourDigitAccountNumber = account.AccountNumber;
            if (account.AccountNumber != null) {
                LastFourDigitAccountNumber = account.AccountNumber.substr(-4);
            }
            if (cashFlowInput != null && cashFlowInput.Transactions != null && cashFlowInput.Transactions != null && cashFlowInput.Transactions.length > 0) {
                SelectedTransactions = cashFlowInput.Transactions.filter(function(trans) {
                    return trans.ProviderAccountId == account.ProviderAccountId;
                });
                if (SelectedTransactions != null && SelectedTransactions.length > 0) {
                    var last6MonthTransactions = [];
                    var lastTransactionOn = SelectedTransactions[0].TransactionDate;
                    var before6MonthTransactionOn = FilterTransactions(lastTransactionOn);
                    SelectedTransactions.forEach(function(objTransaction) {
                        if (new Date(objTransaction.TransactionDate) >= new Date(before6MonthTransactionOn)) {
                            last6MonthTransactions.push(objTransaction);
                        }
                        if (objTransaction.Categories != null && objTransaction.Categories.length > 0) {
                            objTransaction.ScheduleC = objTransaction.Categories[0];
                        }
                    });
                    last6MonthTransactions = [];
                    var totalTransactions = SelectedTransactions.length;
                    SelectedTransactions.sort((a, b) => {
                        var start = +new Date(a.TransactionDate);
                        var elapsed = +new Date(b.TransactionDate) - start;
                        return elapsed;
                    });
                    if (calculateRecurringTransaction == true) {
                        GetRecurringSummary(account.Id, SelectedTransactions, function(trs) {
                            RecurringList = trs;
                        });
                        GetRecurringSummaryByCategoryId(account.Id, SelectedTransactions, function(trs) {
                            MCARecurringList = trs;
                        });
                    }
                    GetCategorySummary(SelectedTransactions, function(s) {
                        CategorySummaryList = s;
                    });
                    GetTransactionList(SelectedTransactions, function(trs) {
                        TransactionList = trs;
                    });
                    EndDate = SelectedTransactions[0].TransactionDate;
                    StartDate = SelectedTransactions[totalTransactions - 1].TransactionDate;
                    var maxTransactionDate = new Date(EndDate);
                    var currentMonth = maxTransactionDate.getMonth();
                    var currentYear = maxTransactionDate.getFullYear();
                    var lastMonth = currentMonth + '-' + currentYear;
                    var minTransactionDate = new Date(StartDate);
                    var firstMonth = minTransactionDate.getMonth() + '-' + minTransactionDate.getFullYear();
                    var isFirstTransaction = true;
                    var isLastMonth = true;
                    var dateOfLastTransaction;
                    var TotalDailyBalanceArray = new Array();
                    var TotalDailyDepositArray = new Array();
                    while (SelectedTransactions.length > 0) {
                        var currentMonthTransactions = SelectedTransactions.filter(function(item) {
                            if (new Date(item.TransactionDate).getMonth() == currentMonth && new Date(item.TransactionDate).getFullYear() == currentYear) {
                                return item;
                            }
                        });
                        if (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length > 0) {
                            var FinicityMonthlyCashFlowViewModel = {
                                Name: '',
                                Year: 0,
                                BeginingBalance: 0.00,
                                EndingBalance: 0.00,
                                DepositCount: 0,
                                WithdrawalCount: 0,
                                TotalDepositAmount: 0.00,
                                TotalWithdrawalAmount: 0.00,
                                AverageDailyBalance: 0.00,
                                FirstTransactionDate: '',
                                EndTransactionDate: '',
                                MinDepositAmount: 0.00,
                                MaxDepositAmount: 0.00,
                                MinWithdrawalAmount: 0.00,
                                MaxWithdrawalAmount: 0.00,
                                NumberOfNSF: 0,
                                NSFAmount: 0.00,
                                NumberOfNegativeBalance: 0.00,
                                CustomAttributes: '',
                                DaysBelow100Count: 0,
                                TotalMonthlyRevenueAmount: 0.00,
                                TotalMonthlyRevenueCount: 0,
                                AverageMonthlyDeposit: 0.00,
                                AverageMonthlyWithdrawal: 0.00,
                                PDFReportName: '',
                                NumberOfSpecialCategoryDeposit: 0,
                                DateOfMonthlyCycle: ''
                            };
                            var currentMonthCashFlow = FinicityMonthlyCashFlowViewModel;
                            var monthlyDailyBalance = new Array(31);
                            var MinDepositAmount = 0,
                                MaxDepositAmount = 0,
                                MinWithdrawalAmount = 0,
                                MaxWithdrawalAmount = 0;
                            var DepositCount = 0,
                                WithdrawalCount = 0,
                                TotalMonthlyDepositAmount = 0,
                                TotalMonthlyWithdrawalAmount = 0;
                            var TotalMonthlyNSFAmount = 0,
                                TotalMonthlyNSFCount = 0;
                            var TotalMonthlyRevenueAmount = 0,
                                TotalMonthlyRevenueCount = 0;
                            var TotalMonthlyNegativeBalanceCount = 0;
                            var AverageMonthlyDeposit = 0.00,
                                AverageMonthlyWithdrawal = 0.00;
                            currentMonthCashFlow.Name = Finicitymonths[currentMonth];
                            currentMonthCashFlow.Year = currentYear;
                            var monthInNumber = (parseInt(currentMonth) + 1).toString();
                            monthInNumber = monthInNumber.length > 1 ? monthInNumber : '0' + monthInNumber;
                            currentMonthCashFlow.PDFReportName = LastFourDigitAccountNumber + '_' + monthInNumber + '_' + currentYear + '.pdf';
                            currentMonthCashFlow.EndingBalance = FinicitycustomRound(beginingBalance, 2);
                            var currentMonthLastTransaction = currentMonthTransactions[0].TransactionDate;
                            currentMonthTransactions.forEach(function(objTransaction) {
                                if (objTransaction != undefined) {
                                    if (beginingBalance < 0) {
                                        NegativeBalanceCount++;
                                        TotalMonthlyNegativeBalanceCount++;
                                    }
                                    if (objTransaction.CategoryId != undefined) {
                                        if (Category_NSF.indexOf(objTransaction.CategoryId) > -1) {
                                            TotalMonthlyNSFAmount = TotalMonthlyNSFAmount + objTransaction.Amount;
                                            TotalMonthlyNSFCount = TotalMonthlyNSFCount + 1;
                                            currentMonthCashFlow.NSFAmount = TotalMonthlyNSFAmount;
                                            currentMonthCashFlow.NumberOfNSF = TotalMonthlyNSFCount;
                                        } else if (Category_Revenue.indexOf(objTransaction.ScheduleC) > -1) {
                                            TotalMonthlyRevenueAmount = TotalMonthlyRevenueAmount + objTransaction.Amount;
                                            TotalMonthlyRevenueCount = TotalMonthlyRevenueCount + 1;
                                            currentMonthCashFlow.TotalMonthlyRevenueAmount = Math.abs(TotalMonthlyRevenueAmount);
                                            currentMonthCashFlow.TotalMonthlyRevenueCount = TotalMonthlyRevenueCount;
                                        } else if (Category_Exclude_Special_Deposite.indexOf(objTransaction.CategoryId) == -1 && Category_Exclude_Special_Deposite.indexOf(objTransaction.ScheduleC) == -1) {
                                            if (objTransaction.Amount > 0) {
                                                currentMonthCashFlow.NumberOfSpecialCategoryDeposit += 1;
                                            }
                                        }
                                    }
                                    var currentTransactionDate = new Date(objTransaction.TransactionDate);
                                    var day = currentTransactionDate.getDate();
                                    var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (getFormattedDate(dateOfLastTransaction) != getFormattedDate(currentTransactionDate));
                                    if (calculateDailyEnding) {
                                        endingBalance = beginingBalance;
                                        monthlyDailyBalance[day - 1] = endingBalance;
                                    }
                                    dateOfLastTransaction = currentTransactionDate;
                                    var transactionAmount = objTransaction.Amount;
                                    if (transactionAmount > 0) {
                                        beginingBalance -= transactionAmount;
                                        TotalDailyDepositArray.push(transactionAmount);
                                        if (transactionAmount > MaxDepositAmount || MaxDepositAmount == 0) {
                                            MaxDepositAmount = transactionAmount;
                                        }
                                        if (transactionAmount < MinDepositAmount || MinDepositAmount == 0) {
                                            MinDepositAmount = transactionAmount;
                                        }
                                        DepositCount = DepositCount + 1;
                                        TotalMonthlyDepositAmount = TotalMonthlyDepositAmount + transactionAmount;
                                    } else {
                                        var tempAmount = Math.abs(transactionAmount);
                                        beginingBalance += tempAmount;
                                        if (tempAmount > MaxWithdrawalAmount || MaxWithdrawalAmount == 0) {
                                            MaxWithdrawalAmount = tempAmount;
                                        }
                                        if (tempAmount < MinWithdrawalAmount || MinWithdrawalAmount == 0) {
                                            MinWithdrawalAmount = tempAmount;
                                        }
                                        WithdrawalCount = WithdrawalCount + 1;
                                        TotalMonthlyWithdrawalAmount = TotalMonthlyWithdrawalAmount + tempAmount;
                                    }
                                    isFirstTransaction = false;
                                    var index = SelectedTransactions.indexOf(objTransaction, 0);
                                    if (index > -1) {
                                        SelectedTransactions.splice(index, 1);
                                    }
                                    TotalDailyBalanceArray.push(beginingBalance);
                                }
                                totalTransactions -= 1;
                            });
                            currentMonthCashFlow.NumberOfNegativeBalance = FinicitycustomRound(TotalMonthlyNegativeBalanceCount, 2);
                            currentMonthCashFlow.TotalWithdrawalAmount = FinicitycustomRound(TotalMonthlyWithdrawalAmount, 2);
                            currentMonthCashFlow.WithdrawalCount = WithdrawalCount;
                            currentMonthCashFlow.TotalDepositAmount = FinicitycustomRound(Math.abs(TotalMonthlyDepositAmount), 2);
                            currentMonthCashFlow.DepositCount = DepositCount;
                            currentMonthCashFlow.MinDepositAmount = Math.abs(MinDepositAmount);
                            currentMonthCashFlow.MaxDepositAmount = Math.abs(MaxDepositAmount);
                            currentMonthCashFlow.MinWithdrawalAmount = MinWithdrawalAmount;
                            currentMonthCashFlow.MaxWithdrawalAmount = MaxWithdrawalAmount;
                            currentMonthCashFlow.AverageMonthlyDeposit = FinicitycustomRound(currentMonthCashFlow.TotalDepositAmount / currentMonthCashFlow.DepositCount, 2);
                            currentMonthCashFlow.AverageMonthlyWithdrawal = FinicitycustomRound(currentMonthCashFlow.TotalWithdrawalAmount / currentMonthCashFlow.WithdrawalCount, 2);
                            currentMonthCashFlow.FirstTransactionDate = FinicitycustomDate(dateOfLastTransaction);
                            if (currentMonthTransactions.length > 0) {
                                currentMonthCashFlow.EndTransactionDate = FinicitycustomDate(new Date(currentMonthTransactions[0].TransactionDate));
                            }
                            currentMonthCashFlow.BeginingBalance = FinicitycustomRound(beginingBalance, 2);
                            var totalDaysInmonth = 0;
                            totalDaysInmonth = finicityGetDaysInMonth(dateOfLastTransaction.getMonth(), dateOfLastTransaction.getFullYear());
                            var monthCheck = currentMonth + '-' + currentYear;
                            if (firstMonth == lastMonth) {
                                currentMonthCashFlow.AverageDailyBalance = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
                                currentMonthCashFlow.DateOfMonthlyCycle = getFormattedDate(dateOfLastTransaction);
                            } else if (lastMonth == monthCheck) {
                                currentMonthCashFlow.AverageDailyBalance = CalculateADBLastMonth(monthlyDailyBalance, currentMonthLastTransaction, TotalDailyBalanceArray);
                                currentMonthCashFlow.DateOfMonthlyCycle = getFormattedDate(currentMonthLastTransaction);
                            } else if (firstMonth == monthCheck) {
                                currentMonthCashFlow.AverageDailyBalance = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
                                currentMonthCashFlow.DateOfMonthlyCycle = GetDateOfMonthlyCycle(dateOfLastTransaction);
                            } else {
                                currentMonthCashFlow.AverageDailyBalance = CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
                                currentMonthCashFlow.DateOfMonthlyCycle = GetDateOfMonthlyCycle(dateOfLastTransaction);
                            }
                            currentMonthCashFlow.DaysBelow100Count = GetDaysBelow100Count(monthlyDailyBalance);
                            MonthlyCashFlowsList.push(FinicitysortObject(currentMonthCashFlow));
                            isLastMonth = false;
                        }
                        currentMonth -= 1;
                        if (currentMonth < 0) {
                            currentYear -= 1;
                            currentMonth = 11;
                        }
                    }
                    var FinicityTransactionSummaryViewModel = {
                        CountOfMonthlyStatement: 0,
                        StartDate: '',
                        EndDate: '',
                        AverageDeposit: 0,
                        AnnualCalculatedRevenue: 0.00,
                        AverageWithdrawal: 0,
                        AverageDailyBalance: 0,
                        NumberOfNegativeBalance: 0,
                        NumberOfNSF: 0,
                        NSFAmount: 0,
                        ChangeInDepositVolume: 0,
                        TotalCredits: 0,
                        TotalDebits: 0,
                        TotalCreditsCount: 0,
                        TotalDebitsCount: 0,
                        AvailableBalance: 0,
                        CurrentBalance: 0,
                        AverageBalanceLastMonth: 0,
                        MedianMonthlyIncome: 0,
                        MedianDailyBalance: 0,
                        MaxDaysBelow100Count: 0,
                        CVOfDailyDeposit: 0,
                        CVOfDailyBalance: 0,
                        AverageMonthlyRevenue: 0.00,
                        TotalRevenueAmount: 0.00,
                        TotalRevenueCount: 0,
                        PDFReportName: ''
                    };
                    var objTransactionSummary = FinicityTransactionSummaryViewModel;
                    objTransactionSummary.PDFReportName = LastFourDigitAccountNumber + '.pdf';
                    objTransactionSummary.CountOfMonthlyStatement = MonthlyCashFlowsList.length;
                    objTransactionSummary.StartDate = getFormattedDate(StartDate);
                    objTransactionSummary.EndDate = getFormattedDate(EndDate);
                    var TotalRevenueAmount = 0,
                        TotalRevenueCount = 0;
                    var TotalDepositAmount = 0;
                    var TotalWithdrawalAmount = 0;
                    var TotalDailyAverageBalance = 0;
                    var TotalNSFAmount = 0;
                    var TotalNSFCount = 0;
                    var TotalDebitsCount = 0,
                        TotalCreditsCount = 0;
                    var TotalMonthlyIncomeArray = new Array();
                    MonthlyCashFlowsList.forEach(function(cashflow) {
                        TotalRevenueAmount += cashflow.TotalMonthlyRevenueAmount;
                        TotalRevenueCount += cashflow.TotalMonthlyRevenueCount;
                        TotalDepositAmount += cashflow.TotalDepositAmount;
                        TotalWithdrawalAmount += cashflow.TotalWithdrawalAmount;
                        TotalDailyAverageBalance += cashflow.AverageDailyBalance;
                        TotalNSFAmount += cashflow.NSFAmount;
                        TotalNSFCount += cashflow.NumberOfNSF;
                        TotalCreditsCount += cashflow.DepositCount;
                        TotalDebitsCount += cashflow.WithdrawalCount;
                        TotalMonthlyIncomeArray.push(cashflow.TotalDepositAmount);
                    });
                    objTransactionSummary.TotalRevenueAmount = FinicitycustomRound((Math.abs(TotalRevenueAmount)), 2);
                    objTransactionSummary.TotalRevenueCount = TotalRevenueCount;
                    objTransactionSummary.AverageMonthlyRevenue = FinicitycustomRound((TotalRevenueAmount / MonthlyCashFlowsList.length), 2);
                    objTransactionSummary.MedianMonthlyIncome = GetMedianSingleArray(TotalMonthlyIncomeArray);
                    objTransactionSummary.MedianDailyBalance = GetMedianSingleArray(TotalDailyBalanceArray);
                    objTransactionSummary.MaxDaysBelow100Count = GetMaxDaysBelow100(MonthlyCashFlowsList);
                    objTransactionSummary.CVOfDailyBalance = FinicitycustomRound(DailyCoEfficient(TotalDailyBalanceArray), 2);
                    objTransactionSummary.CVOfDailyDeposit = FinicitycustomRound(DailyCoEfficient(TotalDailyDepositArray), 2);
                    objTransactionSummary.TotalCredits = TotalDepositAmount;
                    objTransactionSummary.TotalDebits = TotalWithdrawalAmount;
                    objTransactionSummary.TotalCreditsCount = TotalCreditsCount;
                    objTransactionSummary.TotalDebitsCount = TotalDebitsCount;
                    objTransactionSummary.AvailableBalance = account.CurrentBalance;
                    objTransactionSummary.CurrentBalance = account.CurrentBalance;
                    objTransactionSummary.AverageBalanceLastMonth = MonthlyCashFlowsList[0].AverageDailyBalance;
                    objTransactionSummary.AverageDeposit = FinicitycustomRound((TotalDepositAmount / MonthlyCashFlowsList.length), 2);
                    objTransactionSummary.AnnualCalculatedRevenue = FinicitycustomRound((TotalDepositAmount / MonthlyCashFlowsList.length) * 12, 2);
                    objTransactionSummary.AverageWithdrawal = FinicitycustomRound((TotalWithdrawalAmount / MonthlyCashFlowsList.length), 2);
                    objTransactionSummary.AverageDailyBalance = FinicitycustomRound((TotalDailyAverageBalance / MonthlyCashFlowsList.length), 2);
                    objTransactionSummary.NumberOfNegativeBalance = NegativeBalanceCount;
                    objTransactionSummary.NumberOfNSF = TotalNSFCount;
                    objTransactionSummary.NSFAmount = FinicitycustomRound(TotalNSFAmount, 2);
                    var t = myCustomCashFlow;
                    t.MonthlyCashFlows = MonthlyCashFlowsList;
                    t.CategorySummary = CategorySummaryList;
                    t.RecurringList = RecurringList;
                    t.MCARecurringList = MCARecurringList;
                    var objTemp = FinicitysortObject(objTransactionSummary);
                    t.TransactionSummary = objTemp;
                    cashFlowResult.CashFlow = t;
                } else {
                    var FinicityTransactionSummaryViewModel = {
                        CountOfMonthlyStatement: 0,
                        StartDate: '',
                        EndDate: '',
                        AverageDeposit: 0,
                        AnnualCalculatedRevenue: 0.00,
                        AverageWithdrawal: 0,
                        AverageDailyBalance: 0,
                        NumberOfNegativeBalance: 0,
                        NumberOfNSF: 0,
                        NSFAmount: 0,
                        ChangeInDepositVolume: 0,
                        TotalCredits: 0,
                        TotalDebits: 0,
                        TotalCreditsCount: 0,
                        TotalDebitsCount: 0,
                        AvailableBalance: 0,
                        CurrentBalance: 0,
                        AverageBalanceLastMonth: 0,
                        MedianMonthlyIncome: 0,
                        MedianDailyBalance: 0,
                        MaxDaysBelow100Count: 0,
                        CVOfDailyDeposit: 0,
                        CVOfDailyBalance: 0,
                        AverageMonthlyRevenue: 0.00,
                        TotalRevenueAmount: 0.00,
                        TotalRevenueCount: 0,
                        PDFReportName: ''
                    };
                    var objTransactionSummary = FinicityTransactionSummaryViewModel;
                    objTransactionSummary.AvailableBalance = account.CurrentBalance;
                    objTransactionSummary.CurrentBalance = account.CurrentBalance;
                    var t = myCustomCashFlow;
                    var objTemp = FinicitysortObject(objTransactionSummary);
                    t.TransactionSummary = objTemp;
                    cashFlowResult.CashFlow = t;
                }
            } else {
                var FinicityTransactionSummaryViewModel = {
                    CountOfMonthlyStatement: 0,
                    StartDate: '',
                    EndDate: '',
                    AverageDeposit: 0,
                    AnnualCalculatedRevenue: 0.00,
                    AverageWithdrawal: 0,
                    AverageDailyBalance: 0,
                    NumberOfNegativeBalance: 0,
                    NumberOfNSF: 0,
                    NSFAmount: 0,
                    ChangeInDepositVolume: 0,
                    TotalCredits: 0,
                    TotalDebits: 0,
                    TotalCreditsCount: 0,
                    TotalDebitsCount: 0,
                    AvailableBalance: 0,
                    CurrentBalance: 0,
                    AverageBalanceLastMonth: 0,
                    MedianMonthlyIncome: 0,
                    MedianDailyBalance: 0,
                    MaxDaysBelow100Count: 0,
                    CVOfDailyDeposit: 0,
                    CVOfDailyBalance: 0,
                    AverageMonthlyRevenue: 0.00,
                    TotalRevenueAmount: 0.00,
                    TotalRevenueCount: 0,
                    PDFReportName: ''
                };
                var objTransactionSummary = FinicityTransactionSummaryViewModel;
                objTransactionSummary.AvailableBalance = account.CurrentBalance;
                objTransactionSummary.CurrentBalance = account.CurrentBalance;
                var t = myCustomCashFlow;
                var objTemp = FinicitysortObject(objTransactionSummary);
                t.TransactionSummary = objTemp;
                cashFlowResult.CashFlow = t;
            }
            var formattedAccountNumber = account.BankName;
            cashFlowResult.InstitutionName = account.BankName;
            DataAttributePostFix = account.Id;
            cashFlowResult.referenceNumber = payload.eventData.ReferenceNumber;
            cashFlowResult.AccountID = account.Id;
            cashFlowResult.AccountType = account.AccountType;
            cashFlowResult.AccountHeader = formattedAccountNumber + '-' + LastFourDigitAccountNumber + '-' + account.AccountType;
            cashFlowResult.AccountNumber = account.AccountNumber;
            cashFlowResult.Source = account.Source;
            allAccountsCashflows = cashFlowResult;
        }
        var resultKey = DataAttributePostFix;
        var Data = {};
        Data[resultKey] = allAccountsCashflows;
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    }
    var DataAttributePostFix = '';

    function RemoveSpace(str) {
        var result = '';
        var allStr = str.split(' ');
        for (var s = 0; s < allStr.length; s++) {
            result += allStr[s].toString();
        }
        return result;
    }

    function FilterOutlier(trans, handleSuccess) {
        handleSuccess(trans);
    }

    function GetRecurringSummaryByCategoryId(accountId, trans, handleSuccess) {
        var recurringSummary = [];
        var mcaCategories = ['Loan Fees and Charges', 'Loan Insurance', 'Loan Interest', 'Loan Payment', 'Loan Principal', 'Loans'];
        var unsureFlag = 3;
        if (trans != null && trans.length > 0) {
            var allDebitTransactions = trans.filter(function(item) {
                return item.Amount < 0 && mcaCategories.indexOf(item.CategoryId) > -1;
            });
            var moduloGroups = GetGroupByData(allDebitTransactions, 'Amount');
            if (moduloGroups != null) {
                for (var property in moduloGroups) {
                    if (moduloGroups[property].constructor === Array) {
                        var grp = moduloGroups[property];
                        if (grp != null && grp != undefined && grp.length < unsureFlag) {} else {
                            var moduloSubGroups = GetGroupByData(grp, 'Description');
                            if (moduloSubGroups != null) {
                                for (var p in moduloSubGroups) {
                                    if (moduloSubGroups[p].constructor === Array) {
                                        var grpSub = moduloSubGroups[p];
                                        if (grpSub != null && grpSub != undefined && grpSub.length >= unsureFlag) {
                                            var allMatchingTransactions = [];
                                            GetTransactionList(grpSub, function(trs) {
                                                allMatchingTransactions = trs;
                                            });
                                            var temp = {
                                                'Account': accountId,
                                                'Amount': Math.abs(grpSub[0].Amount),
                                                'RoudingAmount': Math.abs(grpSub[0].Amount),
                                                'Transactions': allMatchingTransactions,
                                                'ConfidenceLevel': 'High',
                                                'TotalTransactions': grpSub.length,
                                                'IsRecurring': true,
                                                'Status': 'RECURRING'
                                            };
                                            recurringSummary.push(temp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        handleSuccess(recurringSummary);
    }

    function GetRecurringSummary(accountId, trans, handleSuccess) {
        var recurringSummary = [];
        var moduloRange = 5;
        var roundingMethodType = 2;
        var unsureFlag = 3;
        var minTransactionFlag = 5;
        var dayDifferenceFlag = 7;
        if (trans != null && trans.length > 0) {
            var allDebitTransactions = trans.filter(function(item) {
                return item.Amount < 0;
            });
            for (var i = 0; i < allDebitTransactions.length; i++) {
                var rNumber = GetRoundingAmount(allDebitTransactions[i].Amount, roundingMethodType);
                allDebitTransactions[i].RoudingAmount = rNumber;
                allDebitTransactions[i].RoudingAmountModulus = rNumber % moduloRange;
            }
            var moduloGroups = GetGroupByData(allDebitTransactions, 'RoudingAmountModulus');
            if (moduloGroups != null) {
                for (var property in moduloGroups) {
                    if (moduloGroups[property].constructor === Array) {
                        var grp = moduloGroups[property];
                        if (grp != null && grp != undefined && grp.length <= unsureFlag) {} else {
                            var moduloSubGroups = GetGroupByData(grp, 'RoudingAmount');
                            if (moduloSubGroups != null) {
                                for (var p in moduloSubGroups) {
                                    if (moduloSubGroups[p].constructor === Array) {
                                        var grpSub = moduloSubGroups[p];
                                        if (grpSub != null && grpSub != undefined && grpSub.length >= minTransactionFlag) {
                                            FilterOutlier(grpSub, function(d) {
                                                grpSub = d;
                                            });
                                            grpSub.sort((a, b) => {
                                                var start = +new Date(a.TransactionDate);
                                                var elapsed = +new Date(b.TransactionDate) - start;
                                                return elapsed;
                                            });
                                            for (var i = 0; i < grpSub.length; i++) {
                                                if (grpSub[i + 1] != undefined) {
                                                    var diffDays = GetDaysBetweenTransaction(grpSub[i].TransactionDate, grpSub[i + 1].TransactionDate);
                                                    grpSub[i].diffDays = diffDays;
                                                }
                                            }
                                            var MinValue = GetMinMaxValue(grpSub, 'min');
                                            var MaxValue = GetMinMaxValue(grpSub, 'max');
                                            var rangeDifference = MaxValue - MinValue;
                                            if (rangeDifference > 0) {
                                                if (rangeDifference <= dayDifferenceFlag) {
                                                    var allMatchingTransactions = [];
                                                    GetTransactionList(grpSub, function(trs) {
                                                        allMatchingTransactions = trs;
                                                    });
                                                    var temp = {
                                                        'Account': accountId,
                                                        'Amount': Math.abs(grpSub[0].Amount),
                                                        'RoudingAmount': Math.abs(grpSub[0].RoudingAmount),
                                                        'Transactions': allMatchingTransactions,
                                                        'ConfidenceLevel': 'High',
                                                        'Min': MinValue,
                                                        'Max': MaxValue,
                                                        'MaxMinDiff': rangeDifference,
                                                        'TotalTransactions': grpSub.length,
                                                        'IsRecurring': true,
                                                        'Status': 'RECURRING'
                                                    };
                                                    recurringSummary.push(temp);
                                                } else {}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        handleSuccess(recurringSummary);
    }
    var allowedCategory = ['Loan Fees and Charges', 'Loan Insurance', 'Loan Interest', 'Loan Payment', 'Loan Principal', 'Loans'];

    function GetTransactionList(trans, handleSuccess) {
        var TransactionList = [];
        if (trans != null) {
            for (var t = 0; t < trans.length; t++) {
                var customDate = getCustomFormattedDate(trans[t].TransactionDate);
                var transItem = {
                    'Date': customDate.Date,
                    'Description': trans[t].Description.trim(),
                    'Amount': Math.abs(trans[t].Amount),
                    'TransactionType': trans[t].Amount > 0 ? 'Credit' : 'Debit',
                    'RunningBalance': 0,
                    'Month': customDate.Month,
                    'Year': customDate.Year
                };
                TransactionList.push(transItem);
            }
        }
        handleSuccess(TransactionList);
    }

    function GetCategorySummary(trans, handleSuccess) {
        var predefinedCategoryTransactions = [];
        var CategorySummaryList = [];
        for (var c = 0; c < trans.length; c++) {
            if (allowedCategory.indexOf(trans[c].CategoryId) > -1) {
                predefinedCategoryTransactions.push(trans[c]);
            }
        }
        var temp = [];
        var dayRange = 30;
        var allData = GetCategoryLookup();
        for (var n = 0; n < allData.length; n++) {
            var tempIndex = allData[n].CustomCategoryId;
            temp[tempIndex] = {
                TransactionCount: 0,
                CategoryName: allData[n].Name,
                TransactionTotal: 0.00,
                CategoryId: allData[n].FinicityCategoryId,
                CustomCategoryId: tempIndex,
                LastMonthTransactionCount: 0,
                LastMonthTransactionTotal: 0.00
            };
            CategorySummaryList.push(temp[tempIndex]);
        }
        predefinedCategoryTransactions.reduce(function(res, value) {
            var tIndex = value.CategoryId;
            var objTemp = temp.filter(function(e) {
                return e.CategoryId == tIndex;
            });
            var customIndex = 0;
            if (objTemp != null) {
                if (objTemp.length == 1) {
                    customIndex = objTemp[0].CustomCategoryId;
                } else {
                    if (value.Amount > 0) {
                        customIndex = 4;
                    } else {
                        customIndex = 5;
                    }
                }
                temp[customIndex].TransactionTotal += Math.abs(parseFloat(value.Amount));
                temp[customIndex].TransactionCount += 1;
                if (GetDaysBetweenDate(value.TransactionDate) <= dayRange) {
                    temp[customIndex].LastMonthTransactionTotal += Math.abs(parseFloat(value.Amount));
                    temp[customIndex].LastMonthTransactionCount += 1;
                }
            }
            return temp;
        }, {});
        handleSuccess(CategorySummaryList);
        return CategorySummaryList;
    }

    function GetCategoryLookup() {
        var myCat = [{
            'FinicityCategoryId': 'Loan Fees and Charges',
            'Name': 'Loan Fees and Charges',
            'CustomCategoryId': '1'
        }, {
            'FinicityCategoryId': 'Loan Insurance',
            'Name': 'Loan Insurance',
            'CustomCategoryId': '2'
        }, {
            'FinicityCategoryId': 'Loan Interest',
            'Name': 'Loan Interest',
            'CustomCategoryId': '3'
        }, {
            'FinicityCategoryId': 'Loan Payment',
            'Name': 'Loan Payment',
            'CustomCategoryId': '4'
        }, {
            'FinicityCategoryId': 'Loan Principal',
            'Name': 'Loan Principal',
            'CustomCategoryId': '5'
        }, {
            'FinicityCategoryId': 'Loans',
            'Name': 'Loans',
            'CustomCategoryId': '6'
        }];
        return myCat;
    }

    function GetDaysBetweenTransaction(d1, d2) {
        var date1 = new Date(d1);
        var date2 = new Date(d2);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }

    function GetRoundingAmount(amount, methodType) {
        var result = amount;
        switch (methodType) {
            case 1:
                result = Math.floor(amount);
                break;
            case 2:
                result = Math.ceil(amount);
                break;
        }
        return result;
    }

    function GetGroupByData(data, prop) {
        return data.reduce(function(groups, item) {
            var val = item[prop];
            groups[val] = groups[val] || [];
            groups[val].push(item);
            return groups;
        }, {});
    }

    function GetMinMaxValue(Values, search) {
        var result = 0;
        if (search == 'min') {
            result = 10000;
            for (var i = 0; i < Values.length; i++) {
                if (Values[i].diffDays != undefined && Values[i].diffDays <= result) {
                    if (Values[i].diffDays != 0) {
                        result = Values[i].diffDays;
                    }
                }
            }
        } else if (search == 'max') {
            for (var i = 0; i < Values.length; i++) {
                if (Values[i].diffDays != undefined && Values[i].diffDays >= result) {
                    result = Values[i].diffDays;
                }
            }
        }
        return result;
    }

    function GetDaysBetweenDate(d1) {
        var date1 = new Date(d1);
        var date2 = new Date();
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }

    function getFormattedDate(date) {
        date = new Date(date);
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return month + '/' + day + '/' + year;
    }

    function GetDateOfMonthlyCycle(date) {
        date = new Date(date);
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        return month + '/1/' + year;
    }

    function getCustomFormattedDate(date) {
        date = new Date(date);
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        var MonthName = Finicitymonths[date.getMonth()];
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        var customData = {
            Date: month + '/' + day + '/' + year,
            Month: MonthName,
            Year: year
        };
        return customData;
    }
    var ExcludeWeekEnds = false;

    function isWeekday(year, month, day) {
        var day = new Date(year, month, day).getDay();
        return day != 0 && day != 6;
    }

    function finicityGetDaysInMonth(month, year) {
        var TotalDaysInMonth = new Date(year, month + 1, 0).getDate();
        if (ExcludeWeekEnds == false) {
            return TotalDaysInMonth;
        } else {
            var weekdays = 0;
            for (var i = 0; i < TotalDaysInMonth; i++) {
                if (isWeekday(year, month, i + 1))
                    weekdays++;
            }
            return weekdays;
        }
    }

    function FinicitycustomRound(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }

    function FinicitysortObject(o) {
        var sorted = {},
            key,
            a = [];
        for (key in o) {
            if (o.hasOwnProperty(key)) {
                a.push(key);
            }
        }
        a.sort();
        for (key = 0; key < a.length; key++) {
            sorted[a[key]] = o[a[key]];
        }
        return sorted;
    }

    function Finicitypad(s) {
        return (s < 10) ? '0' + s : s;
    }

    function FinicitycustomDate(d) {
        return [Finicitypad(d.getMonth() + 1), Finicitypad(d.getDate()), d.getFullYear()].join('/');
    }

    function FinicitycalculateAvgBalanceOfMonth(dailyBalanceList, numberOfDaysInMonth) {
        var totalMonthlyDailyBalance = 0;
        var emptyCount = 1;
        for (var index = 30; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
    }

    function CalculateADB(dailyBalanceList, numberOfDaysInMonth, totalDailyBalance) {
        var totalMonthlyDailyBalance = 0;
        var daysCounter = numberOfDaysInMonth - 1;
        var emptyCount = 1;
        for (var index = daysCounter; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        if (emptyCount > 1) {
            var lastDailyBalance = totalDailyBalance[totalDailyBalance.length - 1] * (emptyCount - 1);
            totalMonthlyDailyBalance += lastDailyBalance;
        }
        return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
    }

    function CalculateADBFirstMonth(dailyBalanceList, numberOfDaysInMonth) {
        var totalMonthlyDailyBalance = 0;
        var emptyCount = 1;
        var daysCounter = numberOfDaysInMonth - 1;
        for (var index = daysCounter; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        if (emptyCount > 1) {
            numberOfDaysInMonth = (numberOfDaysInMonth - emptyCount) + 1;
        }
        return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
    }

    function CalculateADBLastMonth(dailyBalanceList, lastTransactionDate, totalDailyBalance) {
        var totalMonthlyDailyBalance = 0;
        var emptyCount = 1;
        var numberOfDaysInMonth = 0;
        var daysCounter = numberOfDaysInMonth - 1;
        if (lastTransactionDate != null && lastTransactionDate != undefined) {
            numberOfDaysInMonth = new Date(lastTransactionDate).getDate();
            daysCounter = numberOfDaysInMonth - 1;
        }
        for (var index = daysCounter; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        if (emptyCount > 1) {
            var lastDailyBalance = totalDailyBalance[totalDailyBalance.length - 1] * (emptyCount - 1);
            totalMonthlyDailyBalance += lastDailyBalance;
        }
        return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
    }

    function GetDaysBelow100Count(data) {
        var count = 0;
        data = RemoveUndefinedData(data);
        for (var c = 0; c < data.length; c++) {
            if (data[c] < 100) {
                count = count + 1;
            }
        }
        return count;
    }

    function RemoveUndefinedData(data) {
        data = data.filter(function(element) {
            return element !== undefined;
        });
        return data;
    }

    function GetMedianSingleArray(data) {
        data = RemoveUndefinedData(data);
        var m = data.sort(function(a, b) {
            return a - b;
        });
        var middle = Math.floor((m.length - 1) / 2);
        if (m.length % 2) {
            return m[middle];
        } else {
            return (m[middle] + m[middle + 1]) / 2.0;
        }
    }

    function GetMaxDaysBelow100(data) {
        return Math.max.apply(Math, data.map(function(o) {
            return o.DaysBelow100Count;
        }));
    }

    function DailyCoEfficient(data, sum, count) {
        Math.mean = function(array) {
            return array.reduce(function(a, b) {
                return a + b;
            }) / array.length;
        };
        Math.stDeviation = function(array, mean) {
            var dev = array.map(function(itm) {
                return (itm - mean) * (itm - mean);
            });
            return Math.sqrt(dev.reduce(function(a, b) {
                return a + b;
            }) / (array.length - 1));
        };
        data = RemoveUndefinedData(data);
        if (data != null && data.length > 0) {
            var average = Math.mean(data);
            var standardDeviation = Math.stDeviation(data, average);
            if (standardDeviation != undefined && !isNaN(standardDeviation)) {
                return (standardDeviation / average) * 100;
            }
            return 0;
        } else {
            return 0;
        }
    }

    function ConvertEPOCDate(d) {
        var date = new Date(d * 1000);
        date = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
        return date;
    }

    function FilterTransactions(startDate) {
        var monthDuration = -5;
        var endDate = new Date(startDate);
        endDate = addMonths(endDate, monthDuration);
        endDate = (endDate.getMonth() + 1) + '/' + '1' + '/' + endDate.getFullYear();
        return endDate;
    }

    function addMonths(date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    }
    return RunCashFlow(payload);
}