function CalculateDrawDownOffer(payload) {

	if (payload != null && payload.drawDown != null) {
		var drawDownData = payload.drawDown[0].DrawDownData;
		var approveAmount = payload.drawDown[0].DrawDownData.RequestedAmount;	

		var Data = {
			'ApproveAmount': approveAmount,
			'InterestRate':10.0,
			'Term' : 12,
			'PaymentFrequency':payload.drawDown[0].applicationData.requestedTermType			
		};
		return {
			'result': 'Passed',
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} else {
		return {
			'result': 'Failed',
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': ['Offer Data not found']
		};
	}
}
