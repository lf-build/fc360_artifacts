function data_attribute_Lenderdecision(payload) {
    var TimeInBiz = 1;
    var YearsDiff = 0;
    var IsNumberOfNSF = true;
    var IsAverageBalance = true;
    var IsAverageDepositSize = true;
    var IsNumberOfDeposit = true;
    var IsZeroBalance = false;
    var MinNumberOfDepositPerMonth = 3;
    var MaxNSF = 12;
    var MinAverageDailyBalance = 1000;
    var MinNumberOfDeposit = 3;
    var AvgMonthlyDeposits = 1000;
    var result = 'Passed';
    var PrestlaData = {
        'lenderName': 'Prestala',
        'statuscode': '100.50'
    };
    var OtherLenderData = {
        'lenderName': 'OtherLenders',
        'statuscode': '600.50'
    };
    function CheckLenderDecision(payload) {
        try {
            if (payload != null && payload.application != null) {
                if (payload.application[0] != null) {
                    YearsDiff = GetYearDiff(new Date(payload.application[0].businessStartDate.Time), new Date());
                    if (YearsDiff < TimeInBiz) {
                        return {
                            'Result': result,
                            'detail': ['Time in Business criteria failed'],
                            'data': OtherLenderData,
                            'rejectcode': '',
                            'exception': []
                        };
                    }
                    if (payload.plaidCashflow != null) {
                        if (payload.plaidCashflow[0].CashFlow != null) {
                            var selectedCashflow = payload.plaidCashflow[0];
                            if (selectedCashflow.CashFlow != null && selectedCashflow.CashFlow.TransactionSummary != null) {
                                var input = selectedCashflow.CashFlow.TransactionSummary;
                                if (input != null) {
                                    if (input.NumberOfNSF > 0) {
                                        if (input.NumberOfNSF > MaxNSF) {
                                            IsNumberOfNSF = false;
                                            result = 'Failed';
                                            return {
                                                'detail': ['IsNumberOfNSF Criteria Failed'],
                                                'data': OtherLenderData,
                                                'rejectcode': '',
                                                'exception': [],
                                                'Result': result
                                            };
                                        }
                                    }
                                    if (input.AverageDailyBalance <= MinAverageDailyBalance) {
                                        IsAverageBalance = false;
                                        result = 'Failed';
                                        return {
                                            'detail': ['Average DailyBalance Criteria Failed'],
                                            'data': OtherLenderData,
                                            'rejectcode': '',
                                            'exception': [],
                                            'Result': result
                                        };
                                    }
                                    if (input.AverageDeposit < AvgMonthlyDeposits) {
                                        IsAverageDepositSize = false;
                                        result = 'Failed';
                                        return {
                                            'detail': ['AverageDeposit Criteria Failed'],
                                            'data': OtherLenderData,
                                            'rejectcode': '',
                                            'exception': [],
                                            'Result': result
                                        };
                                    }
                                    if (input.TotalCreditsCount <= MinNumberOfDeposit) {
                                        IsNumberOfDeposit = false;
                                        result = 'Failed';
                                        return {
                                            'detail': ['MinNumberOfDeposit Failed'],
                                            'data': OtherLenderData,
                                            'rejectcode': '',
                                            'exception': [],
                                            'Result': result
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                return {
                    'detail': null,
                    'data': PrestlaData,
                    'rejectcode': '',
                    'exception': [],
                    'Result': result
                };
            }
            return {
                'detail': null,
                'data': PrestlaData,
                'rejectcode': YearsDiff,
                'exception': [],
                'Result': result
            };
        } catch (e) {
            return {
                'Result': 'Failed',
                'detail': ['Unable to verify : ' + e.message],
                'data': PrestlaData,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    function GetYearDiff(d1, d2) {
        var YearsDiff;
        YearsDiff = (d2.getFullYear() - d1.getFullYear());
        return YearsDiff <= 0 ? 0 : YearsDiff;
    }
    return CheckLenderDecision(payload);
}