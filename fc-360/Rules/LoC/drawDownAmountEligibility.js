function drawDownAmountEligibility(payload) {
    var self = this;


    var dataAttributesService = self.call('dataAttribute');
    var loanAccountingService = self.call('loanAccounting');
    var drawDownService = self.call('drawDown');

    self.result = {
        result: true,
        message: []
    };
    self.isEligible = 'Failed';
    var checkDrawDownAmountEligibility = function () {
        return dataAttributesService.get('application', payload.eventData.applicationNumber, 'product').then(function (productAttributes) {
            if (productAttributes == null || productAttributes[0].ProductParameter == null || productAttributes[0].ProductParameter.lengh <= 0) {
                self.result.result = false;
                self.result.message.push('Product attributes' + ' not found');
                return self.result;
            } else {
                var productParameters = productAttributes[0].ProductParameter;
                var minAmount = 0;
                var firstMinAmountData = productParameters.filter(function (param) {
                    return param.ParameterId == 'MIN_DRAWDOWN_AMOUNT'
                });

                var subsequentMinAmountData = productParameters.filter(function (param) {
                    return param.ParameterId == 'SUBSEQUENT_MIN_DRAWDOWN_AMOUNT'
                });

                return drawDownService.getByLoCNumber(payload.eventData.applicationNumber).then(function (drawDownDetail) {
                    if (drawDownDetail != null && drawDownDetail.length > 0) {
                        if (subsequentMinAmountData != null && subsequentMinAmountData.length > 0) {
                            minAmount = parseFloat(subsequentMinAmountData[0].Value);
                        }
                    }
                    else {
                        if (firstMinAmountData != null && firstMinAmountData.length > 0) {
                            minAmount = parseFloat(firstMinAmountData[0].Value);
                        }
                    }
                    if (parseFloat(payload.eventData.amount) < minAmount) {
                        self.result.result = false;
                        self.result.message.push('Minimum draw down amount should be ' + minAmount);
                        return self.result;
                    }

                    return loanAccountingService.GetAccrualByLoCNumber(payload.eventData.applicationNumber).then(function (loanOutStandingDetail) {
                        if (loanOutStandingDetail != null && loanOutStandingDetail.pbot != null) {
                            var outStandingAmount = parseFloat(loanOutStandingDetail.pbot.totalPrincipalOutStanding);
                            var dealData = payload.eventData.dealData;

                            if (dealData != null) {
                                var creditLimit = parseFloat(dealData.LoanAmount);
                                if (payload.eventData.amount > creditLimit) {
                                    self.result.result = false;
                                    self.result.message.push('Draw down amount exceeds allowable amount');
                                    return self.result;
                                }
                            }
                        }
                        return self.result;
                    }).catch(function () {
                        var dealData = payload.eventData.dealData;
                        if (dealData != null) {
                            var creditLimit = parseFloat(dealData.LoanAmount);
                            if (payload.eventData.amount > creditLimit) {
                                self.result.result = false;
                                self.result.message.push('Draw down amount exceeds allowable amount');
                                return self.result;
                            }
                        }
                        //self.result.result = false;
                        //self.result.message = 'Unable to execute laon accounting service'
                        return Promise.resolve(true);
                    })
                });

                return self.result;

                // for (var i = 0; i < productParameters.length; i++) {
                //     if (productParameters[i].ParameterId == 'MIN_DRAWDOWN_AMOUNT') {
                //         var parameter = productParameters[i];
                //         var firstMinAmount = parameter.Value;
                //         if (parameter.Value != null) {
                //             var minAmount = parseFloat(parameter.Value);
                //             if (parseFloat(payload.eventData.amount) < minAmount) {
                //                 self.result.result = false;
                //                 self.result.message.push('Minimum draw down amount should be ' + minAmount);
                //                 return self.result;
                //             }
                //         }
                //     }

            }
        })
        return self.result;
    }
    if (payload != null && payload.eventData != null) {
        return checkDrawDownAmountEligibility().then(function () {
            if (self.result.result == true || self.result.result == 'true') {
                if (self.result.result == true || self.result.result == 'true') {
                    self.isEligible = 'Passed'
                }
            }
            return {
                'result': self.isEligible,
                'detail': self.result.message,
                'data': null,
                'rejectcode': '',
                'exception': []
            };
        })
    }
    return self.result;
    return {
        'result': 'Failed',
        'detail': self.result.message,
        'data': null,
        'rejectcode': '',
        'exception': []
    };
}