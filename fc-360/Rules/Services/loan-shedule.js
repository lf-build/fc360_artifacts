function loanManagement() {
    var self = this;
    var baseUrl = 'http://172.17.0.1:7092';

    return {
        getLoanSchedule: function (loanNumber) {
            var url = [baseUrl, loanNumber, 'schedule'].join('/');
            return self.http.get(url);
        },

        getLoanNumber: function (applicationNumber) {
            var url = [baseUrl, 'application', applicationNumber].join('/');
            return self.http.get(url);
        }
    };
}