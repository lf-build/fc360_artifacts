function decisionEngine() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7011';
    return {
        executeRule: function(ruleName, locNumber) {
            var url = [baseUrl, ruleName].join('/');
            return self.http.post(url, locNumber);
        }
    };
}