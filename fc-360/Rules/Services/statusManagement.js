function statusManagement() {
    var self = this;
    var baseUrl = '{{internal_ip}}:7021';

    return {
        get: function (entityType, entityId) {
            var url = [baseUrl, entityType, entityId].join('/');
            return self.http.get(url);
        },
        changeStatus: function (entityType, entityId, statusWorkFlowId, statusCode, requestModel) {
            var url = [baseUrl, entityType, entityId, statusWorkFlowId, statusCode].join('/');
            return self.http.put(url, requestModel);
        },
        processStatusWorkFlow: function (entityType, entityId, productId, statusWorkFlowId, status) {
            var url = [baseUrl, entityType, entityId, productId, 'workflow', statusWorkFlowId, status].join('/');
            return self.http.post(url);
        },
        getActiveStatusWorkFlow: function (entityType, entityId) {
            var url = [baseUrl, entityType, entityId, 'statusworkflow'].join('/');
            return self.http.get(url);
        },

    };
}