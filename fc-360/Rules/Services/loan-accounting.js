function loanAccounting() {
    var self = this;
    var baseUrl = 'http://172.17.0.1:7093';

    return {
        GetAccrualByLoCNumber: function (locNumber) {
            var url = [baseUrl, 'accrual',locNumber].join('/');
            return self.http.get(url);
        }
    };
}