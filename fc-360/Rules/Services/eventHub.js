function eventHub() {
	var self = this;
	var baseUrl = 'http://{{internal_ip}}:7002';
	return {
		publish: function (eventName, data) {
			var url = [baseUrl, eventName].join('/');
			return self.http.post(url, data);
		}
	};
}