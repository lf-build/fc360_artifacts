function finicity() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7201';
    return {
        getAccessToken: function(entityType, entityId) {
            var url = [baseUrl, entityType, entityId, 'accesstoken'].join('/');
            return self.http.get(url);
        },
        deleteCustomerAccount: function(entityType, entityId, customerId) {
            var url = [baseUrl, entityType, entityId, 'customer', 'delete'].join('/');
            return this.getAccessToken(entityType, entityId).then(function(response) {
                var payload = {
                    FinicityAppToken: response.accessToken,
                    CustomerId: customerId
                };
                return self.http.post(url, payload);
            });
        },
        enableTxPushCustomerAccount: function(entityType, entityId, customerId, accountId) {
            var url = [baseUrl, entityType, entityId, 'customer', 'account', 'txpush', 'enable'].join('/');
            return this.getAccessToken(entityType, entityId).then(function(response) {
                var payload = {
                    FinicityAppToken: response.accessToken,
                    CustomerId: customerId,
                    AccountId: accountId
                };
                return self.http.post(url, payload);
            });
        },
        disableTxPushCustomerAccount: function(entityType, entityId, customerId, accountId) {
            var url = [baseUrl, entityType, entityId, 'customer', 'account', 'txpush', 'disable'].join('/');
            return this.getAccessToken(entityType, entityId).then(function(response) {
                var payload = {
                    FinicityAppToken: response.accessToken,
                    CustomerId: customerId,
                    AccountId: accountId,
                    CallBackURL: 'http'
                };
                return self.http.post(url, payload);
            });
        }
    };
}