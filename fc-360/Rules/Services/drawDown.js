function drawDown() {
    var self = this;
    var baseUrl = '{{drawdown_internal_url}}';

    return {
        getByLoCNumber: function (locNumber) {
            var url = [baseUrl,locNumber,'loc'].join('/');
            return self.http.get(url);
        }
    };
}