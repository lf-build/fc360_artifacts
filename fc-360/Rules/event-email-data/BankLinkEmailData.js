function BankLinkEmailData(payload) {
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	};
	if (payload && payload.eventData.DataAttributes) {
		if (payload.eventData.DataAttributes.application[0]) {
			var applicationData = payload.eventData.DataAttributes.application[0];
            var eventdata = payload.eventData.EventData;
			var legalBusinessName = jsUcfirst(applicationData.legalBusinessName);
			var email = '';
			var fullName = '';
			var url = '{{server_url}}:{{borrower_port}}/login';
			var queryStringData = '?token=' + applicationData.applicationNumber;
			if (applicationData.owners[0] && applicationData.owners[0].EmailAddresses[0]) {
				email = applicationData.owners[0].EmailAddresses[0].Email;
				fullName = jsUcfirst(applicationData.owners[0].FirstName) + ' ' + jsUcfirst(applicationData.owners[0].LastName);
			}
			 var result = {
				Email: email,
				Name: fullName,
				LegalBusinessName: legalBusinessName,
				Logo: '{{server_url}}:{{server_port}}/static/fc-360/images/logo.png',
				url: url,
				queryString: queryStringData,
				ContactAddress: '{{server_url}}:{{server_port}}/static/fc-360/images/contact_address.jpg'
			};
			return {
				'result': 'Passed',
				'detail': null,
				'data': result,
				'rejectcode': '',
				'exception': []
			};
		}
	}
	  var errorData = [];
	errorData.push('Data Not found for email');
	return {

		'result': 'Failed',
		'detail': null,
		'data': '',
		'rejectcode': '',
		'exception': errorData
	};
}
