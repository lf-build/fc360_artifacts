function rulefortloFactInitiate(payload) {
    var self = this;
    var result = 'Failed';
    var statusManagementService = self.call('statusManagement');
    var mcastatusCode = '500.60';
    var mcalocstatusCode = '700.50';
    var statusMatch = false;
    var Data = null;
    try {
        if (payload != null && payload != undefined) {
            var applicationData = payload.application[0];
            return statusManagementService.getActiveStatusWorkFlow('application', applicationData.applicationNumber).then(function (activeStatusCode) {
                if (mcastatusCode == activeStatusCode.code || mcalocstatusCode == activeStatusCode.code) {
                    statusMatch = true;
                    result = 'Passed';
                    Data = {
                        'StatusMatch': statusMatch
                    };
                }
                return {
                    'result': result,
                    'detail': null,
                    'data': Data,
                    'rejectcode': '',
                    'exception': []
                };
            })
        }
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}