function monthlySchedule(payload) {
	var loanSchedule = [];
	var payOrders = {};
	if (payload.eventData) {
		var term = payload.eventData.term;
		var paymentOrder = payload.eventData.frequency == 'Weekly' ? 4.6 : 23;
		var paybackAmount = payload.eventData.loanAmount;
		var lowerOrder = payload.eventData.loanSchedule[0];
		var higherOrder = payload.eventData.loanSchedule[payload.eventData.loanSchedule.length - 1];
		var lowerPay = lowerOrder.PaymentAmount;
		var higherPay = higherOrder.PaymentAmount;
		payOrders.lowerPay = lowerPay;
		payOrders.higherPay = higherPay;
		var higherOrderMonth = 2;
		for (var i = 0; i < higherOrderMonth; i++) {
			var principle = (lowerOrder.PrincipalAmount * paymentOrder);
			var payment = (lowerOrder.PaymentAmount * paymentOrder);
			var finance = (lowerOrder.InterestAmount * paymentOrder);
			paybackAmount = paybackAmount + finance;
			var payback = paybackAmount;
			var obj = {
				'PrincipalAmount': principle,
				'PaymentAmount': payment,
				'InterestAmount': finance,
				'PaybackAmount': payback
			};
			loanSchedule.push(obj);
		}
		for (var i = higherOrderMonth; i < term; i++) {
			var principle = (higherOrder.PrincipalAmount * paymentOrder);
			var payment = (higherOrder.PaymentAmount * paymentOrder);
			var finance = (higherOrder.InterestAmount * paymentOrder);
			paybackAmount = paybackAmount + finance;
			var payback = paybackAmount;
			var obj = {
				'PrincipalAmount': principle,
				'PaymentAmount': payment,
				'InterestAmount': finance,
				'PaybackAmount': payback
			};
			loanSchedule.push(obj);
		}
	}
	var Data = {
		'loanSchedule': loanSchedule,
		'payOrders': payOrders
	};
	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}