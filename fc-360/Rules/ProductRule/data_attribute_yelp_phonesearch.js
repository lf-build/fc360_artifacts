function data_attribute_yelp_phonesearch(payload) {
	var result = 'Passed';
	var rating = '';
	var isClosed = null;
	var phone = null;
	if (typeof (payload) != 'undefined') {
		var yelpbusinessdetailResponse = payload.eventData.Response.Businesses[0];
		if (yelpbusinessdetailResponse != null) {
			rating = yelpbusinessdetailResponse.Rating;
			isClosed = yelpbusinessdetailResponse.IsClosed;
			phone = yelpbusinessdetailResponse.Phone;
		}

	}
	var Data = {
		'yelpReport': {
			'Rating': rating,
			'IsClosed': isClosed,
			'Phone': phone,
			'referenceNumber': payload.eventData.ReferenceNumber
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
