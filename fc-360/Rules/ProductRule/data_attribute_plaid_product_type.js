function data_attribute_plaid_product_type(payload) {

	function GetFormattedAccessToken(accessToken) {
		return accessToken;
	}

	function ExtractProductType(payload) {
		var result = false;
		var Data = {};
		try {
			if (typeof (payload) != 'undefined') {
				var objInput = payload.eventData.Response;
				if (objInput != null) {
					result = 'Passed';
					var resultKey = 'BankSupportedProductType-' + GetFormattedAccessToken(objInput.BankSupportedProductType);
					Data[resultKey] = objInput.BankSupportedProductType;

					return {
						'result': result,
						'detail': null,
						'data': Data,
						'rejectcode': '',
						'exception': []
					};
				}
			}
			return {
				'result': result,
				'detail': null,
				'data': Data,
				'rejectcode': '',
				'exception': []
			};
		}
		catch (e) {
			return {
				'result': result,
				'detail': null,
				'data': null,
				'exception': [e.message],
				'rejectCode': ''
			};

		}
	}

	return ExtractProductType(payload);

}
