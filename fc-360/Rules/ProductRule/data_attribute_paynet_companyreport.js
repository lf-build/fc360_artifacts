function data_attribute_paynet_companyreport(payload) {
	var result = 'Passed';
	var IsMatch = true;
	var past_Due_31_Plus_Occurrences = '';
	var past_due_31_plus_amt = '';
	if (typeof (payload) != 'undefined') {
		if (payload.eventData.Response.xmlField != null) {
			var paynetResponse = payload.eventData.Response.xmlField.ReportData;
			if (paynetResponse != null) {
				past_Due_31_Plus_Occurrences = parseInt(paynetResponse.Past_Due_31_Plus_Occurrences);
				past_due_31_plus_amt = paynetResponse.PastDue_31_Plus_Amt;
			}
		}
	}
	var Data = {
		'paynetReport': {
			'paynetResult': 'match',
			'Past_due_31_plus_occurrences': past_Due_31_Plus_Occurrences,
			'Past_due_31_plus_amt': past_due_31_plus_amt,
			'referenceNumber': payload.eventData.ReferenceNumber
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}