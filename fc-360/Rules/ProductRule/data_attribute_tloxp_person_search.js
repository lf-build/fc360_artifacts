function data_attribute_tloxp_person_search(payload) {
    var CreditScore = '';
    var Liens = '';
    var Judgments = '';
    var UCCFilings = '';
    var ActiveFromSos = '';
    var Bankruptcies = '';
    var CriminalDetails = '';
    var WarrantDetails = '';
    var PropertyForeclosures = '';
    var result = 'Passed';
    var key = '';
    var owner = null;
    var Data = {};
    if (typeof(payload) !== 'undefined' && payload !== null) {
        var application = payload.application[0];
        if (application !== null && application.owners !== null && application.owners.length > 0) {

            owner = application.owners.filter(function(owner) {
                return ((owner.FirstName === payload.eventData.Request.FirstName) && (owner.SSN === payload.eventData.Request.Ssn));
            });

            key = owner[0].OwnerId;
        }

        var persondetailResponse = payload.eventData.Response;
        if (persondetailResponse !== null) {
            CreditScore = persondetailResponse.CreditScore;
            if (persondetailResponse.Liens !== null && persondetailResponse.Liens !== 'undefined') {
                if (persondetailResponse.Liens.length > 0) {
                    Liens = LatestRecord(persondetailResponse.Liens);
                    if (Liens !== null && Liens !== undefined) {
                        if (Liens.TaxLienDate !== null) {
                            Liens.TaxLienDate = Liens.TaxLienDate.Day + '/' + Liens.TaxLienDate.Month + '/' + Liens.TaxLienDate.Year;
                        }
                    }
                }
            }
            if (persondetailResponse.Judgments !== null && persondetailResponse.Judgments !== 'undefined') {
                if (persondetailResponse.Judgments.length > 0) {
                    Judgments = LatestRecord(persondetailResponse.Judgments);
                }
            }
            if (persondetailResponse.UCCFilings !== null && persondetailResponse.UCCFilings !== 'undefined') {
                if (persondetailResponse.UCCFilings.length > 0) {
                    UCCFilings = LatestRecord(persondetailResponse.UCCFilings);
                }
            }
            ActiveFromSos = persondetailResponse.ActiveFromSos;
            if (persondetailResponse.Bankruptcies !== null && persondetailResponse.Bankruptcies !== 'undefined') {
                if (persondetailResponse.Bankruptcies.length > 0) {
                    Bankruptcies = LatestRecordBankruptcy(persondetailResponse.Bankruptcies);
                    if (Bankruptcies !== null && Bankruptcies !== undefined) {
                        if (Bankruptcies.DischargeDate !== null) {
                            Bankruptcies.DischargeDate = Bankruptcies.DischargeDate.Day + '/' + Bankruptcies.DischargeDate.Month + '/' + Bankruptcies.DischargeDate.Year;
                        }
                    }
                }
            }
            if (persondetailResponse.CriminalRecordsMatch !== null && persondetailResponse.CriminalRecordsMatch !== 'undefined') {
                CriminalDetails = persondetailResponse.CriminalRecordsMatch;
            }
            if (persondetailResponse.PropertyForeclosures !== null && persondetailResponse.PropertyForeclosures !== 'undefined') {
                if (persondetailResponse.PropertyForeclosures.length > 0) {
                    PropertyForeclosures = LatestRecord(persondetailResponse.PropertyForeclosures);
                }
            }
        }
    }
    var records = {
        'CreditScore': CreditScore,
        'Liens': Liens,
        'Judgments': Judgments,
        'UCCFilings': UCCFilings,
        'ActiveFromSos': ActiveFromSos,
        'Bankruptcies': Bankruptcies,
        'CriminalDetails': CriminalDetails,
        'PropertyForeclosures': PropertyForeclosures,
        'SecondaryName': key,
        'referenceNumber': payload.eventData.ReferenceNumber,
        'owner': owner

    };
    Data[key] = records;
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };

    function LatestRecord(data) {
        var latestDate = '';
        var currentDate = '';
        if (data.length > 0) {
            var latestData = data[0];
            if (data[0].FilingDate !== null && data[0].FilingDate.Day !== null) {
                latestDate = new Date(data[0].FilingDate.Year, parseInt(data[0].FilingDate.Month), data[0].FilingDate.Day);
            }
            for (var i = 1; i < data.length; i++) {
                if (data[i].FilingDate !== null) {
                    if (latestDate === '') {
                        latestDate = new Date(data[i].FilingDate.Year, parseInt(data[i].FilingDate.Month - 1), data[i].FilingDate.Day);
                    }
                    currentDate = new Date(data[i].FilingDate.Year, parseInt(data[i].FilingDate.Month) - 1, data[i].FilingDate.Day);
                    if (currentDate >= latestDate && data[i].FilingDate.Day !== null) {
                        latestData = data[i];
                        latestDate = currentDate;
                    }
                }
            }
            if (latestDate !== null) {
                latestData.FilingDateF = latestDate.getDate() + '/' + (parseInt(latestDate.getMonth()) + 1) + '/' + latestDate.getFullYear();
            }
            return latestData;
        }
    }

    function LatestRecordBankruptcy(data) {
        var latestDate = '';
        var currentDate = '';
        if (data.length > 0) {
            var latestData = data[0];
            if (data[0].FileDate !== null && data[0].FileDate.Day !== null) {
                latestDate = new Date(data[0].FileDate.Year, parseInt(data[0].FileDate.Month), data[0].FileDate.Day);
            }
            for (var i = 1; i < data.length; i++) {
                if (data[i].FileDate !== null && data[i].FileDate.Day !== null) {
                    if (latestDate === '') {
                        latestDate = new Date(data[i].FileDate.Year, parseInt(data[i].FileDate.Month - 1), data[i].FileDate.Day);
                    }
                    currentDate = new Date(data[i].FileDate.Year, parseInt(data[i].FileDate.Month) - 1, data[i].FileDate.Day);
                    if (currentDate >= latestDate) {
                        latestData = data[i];
                        latestDate = currentDate;
                    }
                }
            }
            if (latestDate !== null) {
                latestData.FileDateF = latestDate.getDate() + '/' + (parseInt(latestDate.getMonth()) + 1) + '/' + latestDate.getFullYear();
            }
            return latestData;
        }
    }
}